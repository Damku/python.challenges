amount = float(input("Enter initial amount: "))
rate = float(input("Enter annual rate of return: "))
years = int(input("Enter years: "))


def invest(amount, rate, years):
    for years in range(1, years + 1):
        result = amount * rate + amount
        amount = result
        print(f"Year {years}: ${result:,.2f}")

invest(amount, rate, years)
