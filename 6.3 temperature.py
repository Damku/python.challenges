def convert_cel_to_far(C):
    conversion = round(C * (9/5) + 32, 2)
    return conversion
def convert_far_to_cel(F):
    conversion = round((F - 32) * (5/9), 2)
    return conversion
C_input = float(input("Enter a tempteratur in degrees C: "))
cel_to_far_result = convert_cel_to_far(C_input)
print(f"{C_input} degrees C = {cel_to_far_result} degrees F")
F_input = float(input("Enter a temperature in degrees F: "))
far_to_cel_result = convert_far_to_cel(F_input)
print(f"{F_input} degrees F = {far_to_cel_result} degrees C")
