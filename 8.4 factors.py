# Pierwszy przykład
number = int(input("Enter a positive integer: "))
factor = 1

while factor <= number:
    if (number % factor) == 0:
        print(f"{factor} is a factor of {number}")

    factor = factor + 1

# Drugi przykład
num = int(input("Enter a positive integer: "))

for n in range(1, num + 1):
    if num % n == 0:
        print(f"{n} is a factor of {num}")
