principal_amount = float(input("Enter initial deposit: "))
annual_rate = float(input("Enter annual rate of return: "))
years = int(input("Enter years of investment: "))


def invest(principal_amount, annual_rate, years):
    interest = principal_amount * annual_rate * (years / years)
    return interest


for years in range(int(years / years), int(years + 1)):
    interest = invest(principal_amount, annual_rate, years)
    total_profit = principal_amount + interest
    print(f"Year {years}: ${total_profit:,.2f}")
    principal_amount = total_profit
